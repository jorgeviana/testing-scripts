# Testing Scripts Using Javascript

## Watch the video
https://youtu.be/SiiQNCCRbCo

## Install dependencies
```
npm install
```

## Run tests in watch mode
```
npm run watch
```

## TODO
- Extract test auxiliary functions
- Refactor tests to data/table format, will read better
