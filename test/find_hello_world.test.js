import { 
  pipe,
  runScript,
  expectResult
} from "./test_functions.js"


describe('the script find_hello_world', () => {
  
  it('should print the line number that contains "Hello, World!"', async () => {
    // given
    const text = `
hi
Hello, World!
hello world
`
    // when piping the text to the script
    const result = await pipe(text, 'src/find_hello_world')

    // then
    expectResult(result, `
2
`)
  })


  it('should work with arguments', async () => {
    // given
    const fileName = 'test/hello.txt'

    // when running the script with a file name as argument
    const result = await runScript('src/find_hello_world', fileName)

    // then
    expectResult(result, `
2
`)
  })
})
