import { 
  pipe,
  runScript,
  expectResult
} from "./test_functions.js"


describe('sed substitution to solve one of github actions deprecation', () => {

  it('should transform not quoted', async () => {
    // given
    const text = `
echo ::set-output name={--NAME--}::{--VALUE--}
`
    // when
    const result = await pipe(text, 'src/sed-substitution')

    // then
    expectResult(result, `
echo "{--NAME--}={--VALUE--}" >> $GITHUB_OUTPUT
`)
  })


  it('should transform quoted', async () => {
    // given
    const text = `
echo "::set-output name={name}::{value}"
`
    // when
    const result = await pipe(text, 'src/sed-substitution')

    // then
    expectResult(result, `
echo "{name}={value}" >> $GITHUB_OUTPUT
`)
  })


  it('should transform only relevant text', async () => {
    // given
    const text = `
- name: Set output
  run: echo ::set-output name={name}::{value}
`
    // when
    const result = await pipe(text, 'src/sed-substitution')

    // then
    expectResult(result, `
- name: Set output
  run: echo "{name}={value}" >> $GITHUB_OUTPUT
`)
  })


  it('should transform only relevant text, quoted', async () => {
    // given
    const text = `
- name: Set output
  run: echo "::set-output name={name}::{value}"
`
    // when
    const result = await pipe(text, 'src/sed-substitution')

    // then
    expectResult(result, `
- name: Set output
  run: echo "{name}={value}" >> $GITHUB_OUTPUT
`)
  })


  it('corner case where a quote needs to be removed...', async () => {
    // given
    const text = `
"" >> $GITHUB_OUTPUT "" >> $GITHUB_OUTPUT
`
    // when
    const result = await pipe(text, 'src/sed-substitution')

    // then
    expectResult(result, `
"" >> $GITHUB_OUTPUT " >> $GITHUB_OUTPUT
`)
  })


  it('should run with argument', async () => {
    // given
    const file = 'test/sed.txt'

    // when
    const result = await runScript('src/sed-substitution', file)

    // then
    expectResult(result, `
- name: Set output
  run: echo "{name}={value}" >> $GITHUB_OUTPUT
`)
  })
})
