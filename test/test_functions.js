import { expect } from "chai"
import { $ } from "zx"

function expectNoErrors(result) {
  expect(result.stderr).to.equal('')
  expect(result.exitCode).to.equal(0)
}

export function pipe(text, script) {
  return $`printf "%s" ${text.replace('\n', '')} | ${script}`
}

export function runScript(script, file) {
  return $`${script} ${file}`
}

export function expectResult(result, expected) {
  expectNoErrors(result)
  expect(result.stdout).to.equal(expected.replace('\n', ''))
}

// function myExpectIdent(result, expected) {
//     expectNoErrors(result)
//     expect(result.stdout).to.equal(
//         expected
//           .replace('\n', '')
//           .replaceAll(/^\s+\|/gm, '')
//     )
// }
